# Dockerfiles

This is the portable config of my linux environment.
Using docker to preserve the host environment for as long as possible.

## Usage

### `make install`

```sh
make [install]
```

Build all images and copy all wraper scripts to `/usr/local/bin`.

### `make <tool>`

```sh
make <tool>
```

Build image `tool` and copy its wraper script to `/usr/local/bin`.

### `make uninstall`

```sh
make uninstall
```

Remove all docker images and their respective wraper script installed by the make file.

### `make uninstall-<tool>`

```sh
make uninstall-<tool>
```

Remove image `tool` and its wraper script.
