OUT_PATH := /usr/local/bin
IMAGE_NAME := $(notdir $(shell pwd))

DOCKERFILE := $(shell [ -z "$$(docker images | grep '$(IMAGE_NAME)')" ] && find . -maxdepth 1 -iname Dockerfile || echo)
IMAGE_TARGET := $(shell [ -n "$(DOCKERFILE)" ] && echo build || echo)

SCRIPT := $(shell find . -maxdepth 1 -iname main)
SCRIPT_TARGET := $(shell [ -f '$(SCRIPT)' ] && echo $(OUT_PATH)/$(IMAGE_NAME) || echo)

include ../../make/common-functions.mk

.PHONY: all
all: install

.PHONY: install $(IMAGE_TARGET)
install: $(IMAGE_TARGET) $(SCRIPT_TARGET)
$(IMAGE_TARGET): $(DOCKERFILE)
	@printf '\033[32m\n%s\n\033[0;39m' '[building image $(IMAGE_NAME)]' && \
	BUILD_ARGS="$$( \
		grep '^ARG' $< | \
		awk -F' |=' $$'{ \
			print "--build-arg " $$2 "=\\"$$( \
				read -p \047" $$2 " = \047 arg && printf \047%s\047 \\"$${arg}\\" \
			)\\"" \
		}' \
	)" && \
	sh -c "docker build -t $(IMAGE_NAME) $${BUILD_ARGS//$$'\n'/ } ."

$(SCRIPT_TARGET): $(SCRIPT)
	@cp $< $@ && \
	chmod +x $@

UNINSTALL_SCRIPT := $(shell [ -f '$(SCRIPT_TARGET)' ] && echo $(call uninstall,$(SCRIPT_TARGET)) || echo)
UNINSTALL_IMAGE := $(shell [ -n "$$(docker images | grep '$(IMAGE_NAME)')" ] && echo $(call uninstall,$(IMAGE_NAME)) || echo)

.PHONY: uninstall
uninstall: $(UNINSTALL_SCRIPT) $(UNINSTALL_IMAGE)

$(call uninstall,$(SCRIPT_TARGET)):
	@rm $(SCRIPT_TARGET)

$(call uninstall,$(IMAGE_NAME)):
	@docker rmi $(IMAGE_NAME)
