#!/bin/sh
set -e

read -p 'image name: ' IMAGE_NAME
[ ! -f "${CONFIG_PATH}"/"${IMAGE_NAME}" ] || (echo 'config already exists' && false)

read -p 'image tag [latest]: ' IMAGE_TAG
IMAGE_TAG="${IMAGE_TAG:-latest}"

IMAGE_IDENTIFIER="${IMAGE_NAME}:${IMAGE_TAG}"
read -p "creating config for ${IMAGE_IDENTIFIER}. iz dat okey? [Y/n] " CONFIRMATION
[ -z "${CONFIRMATION}" ] || [ "${CONFIRMATION}" -eq 'y' ] || [ "${CONFIRMATION}" -eq 'Y' ]

cp -r --no-target-directory template "${CONFIG_PATH}"/"${IMAGE_NAME}"
cd "${CONFIG_PATH}"/"${IMAGE_NAME}"
for CONFIG_FILE in *; do
    sed -i "s/\${IMAGE}/${IMAGE_IDENTIFIER}/g" "${CONFIG_FILE}"
done
