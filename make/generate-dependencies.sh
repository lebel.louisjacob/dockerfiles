#!/bin/sh
set -e

DEPENDENCIES_FILE="${1}"
shift

for DOCKERFILE in "${@}"; do
    IMAGE_NAME="$(basename "$(dirname "${DOCKERFILE}")")";
    BASE_IMAGE_NAME="$(grep 'FROM' ${DOCKERFILE} | head -n 1 | awk -F' |:' '{print $2}')";
    if [ -e "${CONFIG_PATH}"/"${BASE_IMAGE_NAME}" ]; then
        DEPENDENCIES="${DEPENDENCIES}${IMAGE_NAME}"': '"${BASE_IMAGE_NAME}"$'\n'
    fi
done

mkdir -p "$(dirname "${DEPENDENCIES_FILE}")"
printf '%s' "${DEPENDENCIES}" > "${DEPENDENCIES_FILE}"
